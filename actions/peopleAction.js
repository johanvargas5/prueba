import Axios from 'axios';
import * as types from './types';
import { callServiceError } from './globalActions';
import apiInit from './init'; 


export function getPeople( page ) {
    return ( dispatch ) => {
        apiInit(dispatch );
        Axios.get(
          page,
        ).then( response => {
			
           return dispatch( peopleDone(response.data) );
        })
        .catch( error =>  { 
			
            return dispatch(callServiceError( { message: error  }))
         })
    }
}

export function peopleDone( payload ) {
	
    return{ 
    type : types.PEOPLE_DONE, 
    payload
  };
}



export function getPersonage( id ) {
    return ( dispatch ) => {
        apiInit(dispatch );
        Axios.get(
           'https://swapi.co/api/people/'+id,
        ).then( response => {
           return dispatch(personageDone(response.data) );
        })
        .catch( error =>  { 
			
            return dispatch(callServiceError({ message: error  }))
         })
    }
}

export function personageDone( payload ) {
	
    return{ 
    type : types.PERSONAGE_DONE, 
    payload
  };
}

export function reload(  ) {
	
    return{ 
    type : types.RELOAD

  };
}