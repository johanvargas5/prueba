import * as types from './types';

export function callServiceBegin(){

  return { 
    type : types.CALL_SERVICE_BEGIN
  };
}

export function callServiceError( payload ){

  return { 
    type : types.CALL_SERVICE_ERROR,
    payload
  };
}



