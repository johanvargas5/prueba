import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { getPeople, reload } from './actions/peopleAction'
import { connect } from 'react-redux'
import { Table, Button, Icon, Spin , Row , message} from 'antd';

import { Link, browserHistory } from 'react-router'
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

class Home extends React.Component {
   constructor(props){
    super(props);
    this.state = {
        people: [],
        serviceInfo: {next :'https://swapi.co/api/people/' },
        personage: {},
        isCallError: false,
	      callErrorMessage: '',
		    isLoading: true,
        filteredInfo: null,
        sortedInfo: null,
    } 

  }

  componentWillMount(){
    
    if (this.props.peopleReducer.people.results.length === 0 )
        this.props.getPeople( this.state.serviceInfo.next) 
    else  
       this.props.reload() 
  }

  componentWillReceiveProps(nextProps){
  
    this.setState({ 
      people: nextProps.peopleReducer.people.results.map((item, index,arr)=> {
           if (item.species[0] != null ){ 
           var clearAll = item.species[0].replace( 'https://swapi.co/api/species/', '')
           var nuevaCadena = clearAll.replace( '/', '')
           item.species = nuevaCadena
           }
        return item
      }),
      serviceInfo: nextProps.peopleReducer.people,
      personage: nextProps.peopleReducer.personage,
      isCallError: nextProps.handlerGeneralReducer.isCallError,
	    callErrorMessage:  nextProps.handlerGeneralReducer.callErrorMessage,
	  	isLoading: nextProps.handlerGeneralReducer.numServiceCallsInProgress > 0 ? true : false,
    })
     if(nextProps.handlerGeneralReducer.isCallError){
            message.error(""+nextProps.handlerGeneralReducer.callErrorMessage);
            this.setState({  isCallError: false,
                callErrorMessage: ''})
      }
      
  }
  
   next(context){
      if(this.state.serviceInfo.next)
        this.props.getPeople(this.state.serviceInfo.next)  
  }
  
  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  }

  clearFilters = () => {
    this.setState({ filteredInfo: null });
  }

  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null,
    });
  }

  setSpeciesSort = () => {
    this.setState({
      sortedInfo: {
        order: 'descend',
        columnKey: 'species',
      },
    });
  }
  render(){
    
    
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};

    const columns = [
       {
      title: 'gender',
      dataIndex: 'gender',
      key: 'gender',
      filters: [
      ],
      filteredValue: filteredInfo.address || null,
      onFilter: (value, record) => record.address.includes(value),
      render: (gender, record) => {
          var { icon, color } =  gender == 'male' ? { icon: 'man', color: 'blue'} :  
          gender  == 'female' ?  { icon: 'woman', color: 'red'}  :  
          { icon: 'pause', color: 'gray'}
        
          return ( <Icon type={icon} style={ {color: color}}/>)
        }
    },{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      filters: [
       
      ],
      filteredValue: filteredInfo.name || null,
      render : (item, record)=>{
         var clearAll = record.url.replace( 'https://swapi.co/api/people/', '')
         var id = clearAll.replace( '/', '')
          return <span onClick={()=>{ 	browserHistory.push({ pathname: '/personage/', search: id})}} > {item}</span>
      }
    }, {
      title: 'height',
      dataIndex: 'height',
      key: 'height',
      sorter: (a, b) => a.height - b.height,
      sortOrder: sortedInfo.columnKey === 'height' && sortedInfo.order,
    }, {
      title: 'species',
      dataIndex: 'species',
      key:  'species',
      sorter: (a, b) => a.species - b.species,
      sortOrder: sortedInfo.columnKey === 'species' && sortedInfo.order,
  
    }];

    return (
    <div style={{ margin: 100 }}>
        <h1>Api StarWars. Prueba de implementación.</h1>
        <hr /><br />
        <div>
              <div className="table-operations">
                  <Button onClick={this.setSpeciesSort}>Ordenar por especies</Button>
                  <Button onClick={this.clearAll}>Limpiar orden</Button>
              </div>
                { this.state.isLoading ? 
              <Spin style= {{  marginTop:20, marginLeft : 100}}indicator={antIcon} /> : 
              <div>
              
                <Table  rowKey={record => record.url}  columns={columns} dataSource={this.state.people} onChange={this.handleChange} />
                <Button onClick={this.next.bind(this)} style={{ marginRight: 10  }}>NEXT PAGE</Button>
              </div>
              }
              
        </div>
    </div>
    );
  }
}

function mapStateToProps(state){

  return {
   	handlerGeneralReducer : state.handlerGeneralReducer,
    peopleReducer:state.peopleReducer
  }
}

export default connect( mapStateToProps, {  getPeople, reload  } )( Home )

