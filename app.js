import React from 'react'
import { Provider } from 'react-redux'
import Routes from './routes'
import configureStore from './store/configureStore'

const store = configureStore()

function RouterConfig() {
	return (
	<Provider store={ store } >
			<Routes />
	</Provider>
	);
}

export default RouterConfig;
