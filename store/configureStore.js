import { compose, createStore, applyMiddleware } from 'redux'
import reducers from '../reducers'
import thunk from 'redux-thunk'

//const middleWare = [thunk]

//const createStoreWithMiddleware = applyMiddleware(...middleWare)(createStore)
export default function configureStore() {
	
	return (compose(applyMiddleware(thunk) )(createStore)(reducers))
	//return createStoreWithMiddleware(reducers)
}

