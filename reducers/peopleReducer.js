import * as types from '../actions/types';


var initialState = {
  personage :[],
  people :{ 
    count: 0, 
    next: "", 
    previous: null, 
    results: []
   },
  
}

function peopleReducer( state = initialState, action ) {
 switch ( action.type ) {
   case types.PEOPLE_DONE:
       action.payload.results = state.people.results.concat( action.payload.results )  
     return { ...state, people: action.payload}; 
   case types.PERSONAGE_DONE:
     return { ...state, personage: [action.payload]};
    
    case types.RELOAD:
     return { ...state, previous: true};

   default:
  
     return state;
 }
}

export default peopleReducer