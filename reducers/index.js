import { combineReducers } from 'redux'

import handlerGeneralReducer from './handlerGeneralReducer'
import peopleReducer from './peopleReducer'

const reducers = combineReducers({
   
   handlerGeneralReducer,
   peopleReducer
})

export default reducers