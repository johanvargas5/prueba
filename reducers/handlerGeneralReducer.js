import * as types from '../actions/types';
import initialState from './initialState';

function actionTypeEndsInSuccess( type ) {
  return type.substring( type.length - 5 ) === '_DONE';
}

export default function ajaxStatusReducer( state = initialState, action ) {
 
  if( actionTypeEndsInSuccess( action.type ) ) {
   
    return { ...state, 
        numServiceCallsInProgress : state.numServiceCallsInProgress - 1 , 
        isCallError :false,
        callErrorMessage :''
      };
  }

  switch ( action.type ) {
    case types.CALL_SERVICE_BEGIN:
      return { 
        ...state, 
        numServiceCallsInProgress : state.numServiceCallsInProgress + 1,
        isCallError :false,
        callErrorMessage :''
        
      };
    
    case types.CALL_SERVICE_ERROR:
  
      return { 
        ...state, 
        numServiceCallsInProgress : state.numServiceCallsInProgress - 1,
        isCallError :  true, 
        callErrorMessage : action.payload.message
      };
 
    default:
      return state;
  }
}