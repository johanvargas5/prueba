import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {  getPersonage } from './actions/peopleAction'
import { connect } from 'react-redux'
import { Table, Button, Icon, List,Spin, message } from 'antd';

import {  browserHistory } from 'react-router'

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

class Personage extends React.Component {
   constructor(props){
    super(props);
    this.state = {
        personage: [],
        isCallError: false,
	      callErrorMessage: '',
		    isLoading: true,
        filteredInfo: null,
        sortedInfo: null,
    } 

  }

  componentWillMount(){
    this.props.getPersonage(this.props.location.search)
  }

  componentWillReceiveProps(nextProps){
     var data = []
     // procesamiento de data para mostrar la lista dinamica
     if( nextProps.peopleReducer.personage.length > 0 ){ 
        var  personage = nextProps.peopleReducer.personage[0]
        for (var key in personage){
            var obj ={}
            obj.title = key;
            obj.description = personage[key];
            data.push(obj)
        }
    }
    this.setState({ 
      
      personage:data,
      isCallError: nextProps.handlerGeneralReducer.isCallError,
	    callErrorMessage:  nextProps.handlerGeneralReducer.callErrorMessage,
	    isLoading: nextProps.handlerGeneralReducer.numServiceCallsInProgress > 0 ? true : false,
    })
     if(nextProps.handlerGeneralReducer.isCallError){
            message.error(""+nextProps.handlerGeneralReducer.callErrorMessage);
            this.setState({  isCallError: false,
                callErrorMessage: ''})
      }
      
  }

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  }

  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null,
    });
  }
  setSpeciesSort = () => {
    this.setState({
      sortedInfo: {
        order: 'descend',
        columnKey: 'species',
      },
    });
  }
  render(){
    

    return (
    <div style={{ margin: 100 }}>
      <h1>Personje.</h1>
      <hr /><br />
      
       <div>
            <div className="table-operations">
            </div>
              { this.state.isLoading ? 
              <Spin style= {{  marginTop:20, marginLeft : 100}}indicator={antIcon} /> :       
              <List
                  itemLayout="horizontal"
                  dataSource={this.state.personage}
                  renderItem={item =>  ( <List.Item>
                              <List.Item.Meta
                      
                              title={item.title}
                              description={item.description}
                              />
                          </List.Item>
                      )}
                  
              /> 
              }
        </div>
    </div>
    );
  }
}

function mapStateToProps(state){

  return {
   	handlerGeneralReducer : state.handlerGeneralReducer,
    peopleReducer:state.peopleReducer
  }
}

export default connect( mapStateToProps, {   getPersonage  } )( Personage )